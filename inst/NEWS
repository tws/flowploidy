
Changes in version 0.99.2 (2016-10-12)
--------------------------------------

Internal Changes

    * added a new slot to ModelComponents, `paramLimits`, which allows
      lower and upper limits to be set for each model parameter. (corrects
      bug where linearity wanders below 1, giving nonsense results).

    * rationalized the bounds of the data fit in the NLS procedure. Model
      fitting, and RCS calculation, are now all tied to the bin identified
      by fhStart. This selects the highest intensity (peak) in the first 20
      non-zero channels, and ignores all channels below this point. Prior
      to this, the number of observations and associated degrees of freedom
      was calculated in an ad-hoc manner, making the RCS values (even-more)
      difficult to interpret; in addition, the single- and multi- cut
      values started at one channel, but the RCS calculations started on
      another channel, which didn't make sense.

Changes in version 0.99.1 (2016-10-11)
--------------------------------------

User Visible Changes

    * Vignette updated to include installation instructions for
      BioConductor

Internal Changes

    * Added accessor functions for ModelComponent and FlowHist classes, so
      direct access of slots via the `@` operator is no longer used outside
      of the initialization functions.

    * replaced some loops with vectorized calculations

    * replaced call to `eval` with a normal function call to `nlsLM` in
      flowAnalyze.R.

    * formatted NEWS file

Changes in version 0.99.0 (2016-08-25)
--------------------------------------

    * What's NEW? Everything so far!
